module ProgressiveBinningOptim

"""
    findnearest(x::Number, ys::AbstractVector)

Find the index of `y` in `ys` that is closest to `x`.
"""
function findnearest(x::Number, ys::AbstractVector)
    argmin(@. abs(ys - x))
end

"""
    remapper(indims::Tuple, outdims::Tuple)

Return an array of `CartesianIndex` that will interpolate an array of
size `indims` to one of size `outdims`.
"""
function remapper(indims::Tuple, outdims::Tuple)
    @assert length(indims) == length(outdims)
    inaxes = LinRange.(0, 1, indims)
    outaxes = LinRange.(0, 1, outdims)
    nearestidxs = [
        [findnearest(o, inaxis) for o in outaxis] for
        (inaxis, outaxis) in zip(inaxes, outaxes)
    ]
    CartesianIndex.(Iterators.product(nearestidxs...))
end

"""
    progressive_binning_optim(X0::AbstractArray, costfun::Function, optfun::Function)

Iteratively run optimization routine `optfun(costfun, X)` using cost
function `costfun(X)` where `X` is increased from a small array (re-mapped
to an appropriately size array) to a larger array until finally optimizing
over full-sized array equal in size to `X0`.
"""
function progressive_binning_optim(X::AbstractArray, costfun::Function, optfun::Function)
    # Define current and next dimensions for optimized array
    # Start optimizing an array with 2-elements per axis
    dims_current = Tuple(ones(Int, ndims(X))) .* 2
    dims_next = dims_current .* 2
    # Initial best-performing array
    best = similar(X, dims_current)
    # Iterate doubling the dimension on each edge at every iteration
    for i = 1:Int(floor(log2(minimum(size(X)))))
        @info string("Optimizing at dimensions of ", dims_current)
        # Create the re-mapping array of indices to transform the smaller
        # array being optimized to the full-size input to the cost function
        rm = remapper(dims_current, size(X))
        # Optimize the at the current size of input (input will be upscaled)
        best = optfun(x -> costfun(x[rm]), best)
        @info string("Optimized at dimensions of ", dims_current)
        # Upscale the best result and move to a finer grid for the next iteration
        if all(dims_next .<= size(X))
            best = best[remapper(dims_current, dims_next)]
            dims_current = dims_next
            dims_next = dims_current .* 2
        end
    end
    # Re-scale to the correct final dimensions
    best = best[remapper(dims_current, size(X))]
    @info string("Optimizing at final dimensions of ", size(best))
    # And run the final optimization at the full dimensions
    best = optfun(costfun, best)
    @info string("Optimize at final dimensions of ", size(best))
    best
end

export progressive_binning_optim

end
