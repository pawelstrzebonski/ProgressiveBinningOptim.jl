using Documenter
import ProgressiveBinningOptim

makedocs(
    sitename = "ProgressiveBinningOptim.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "ProgressiveBinningOptim.jl"),
    pages = [
        "Home" => "index.md",
        "Examples" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => ["ProgressiveBinningOptim.jl" => "ProgressiveBinningOptim.md"],
        "test/" => ["ProgressiveBinningOptim.jl" => "ProgressiveBinningOptim_test.md"],
    ],
)
