# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Better examples of usage
* Optimizations
* More configurations/options (such as custom scaling factor for each iteration, better heuristics for up-scaling, interpolating up-scaling)

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support arbitrary sized and dimension inputs
* We aim to be (more-or-less) optimizer agnostic
* We aim to minimize the amount of code and function repetition
