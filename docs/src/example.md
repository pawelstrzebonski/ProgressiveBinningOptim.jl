# Example

This package does not optimize itself, but rather wraps around a provided
optimization routine. First, we'll want to prepare the external optimization
routine and associated functions:

```Julia
# Import the optimization package
import ...

# Define the cost-function (that quantifies how good an array is)
costfun(x) = ...

# Define an example input array (that will be optimized)
x0 = ...

# Wrap the optimization routine call to get the correct convention for ProgressiveBinningOptim
# In this case, the provided function should take two arguments, first
# the cost function `f`, and second an example of the input array being optimized `x0`.
# It should return the optimized array. It should work for any size of array.
optfun(f, x0) = ...
```

Now that we've defined the "back-end" code for the optimizer, we can
use the `ProgressiveBinningOptim` code:

```Julia
# Import the package
import ProgressiveBinningOptim

# Run progressive optimizer
xopt = ProgressiveBinningOptim.progressive_binning_optim(x0, costfun, optfun)
```

The return value `xopt` is the final optimized array.
