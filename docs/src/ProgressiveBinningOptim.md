# ProgressiveBinningOptim.jl

## Description

This file implements all of this package's functionality.

## Functions

```@autodocs
Modules = [ProgressiveBinningOptim]
Pages   = ["ProgressiveBinningOptim.jl"]
```
