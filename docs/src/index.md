# ProgressiveBinningOptim Documentation

## About

`ProgressiveBinningOptim.jl` is a Julia package to help optimize large
arrays by iteratively running an optimization routine on a smaller
version of the array, upscaling it to the full size, and using that
as the starting point for the next refinement (larger size) optimization
call. This package does not implement an optimization routine itself,
but requires one to be provided.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/ProgressiveBinningOptim.jl
```

## Features

* Progressively decreased binning optimization helper functions

## Related Packages

* [PseudoBooleanOptim.jl](https://gitlab.com/pawelstrzebonski/PseudoBooleanOptim.jl) can be used in conjunction with this package to optimize large binary arrays (eg binary images)
