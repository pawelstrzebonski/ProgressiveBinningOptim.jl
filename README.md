# ProgressiveBinningOptim

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/ProgressiveBinningOptim.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/ProgressiveBinningOptim.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/ProgressiveBinningOptim.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/ProgressiveBinningOptim.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/ProgressiveBinningOptim.jl/commits/master)

## About

`ProgressiveBinningOptim.jl` is a Julia package to help optimize large
arrays by iteratively running an optimization routine on a smaller
version of the array, upscaling it to the full size, and using that
as the starting point for the next refinement (larger size) optimization
call. This package does not implement an optimization routine itself,
but requires one to be provided.

## Features

* Progressively decreased binning optimization helper functions

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/ProgressiveBinningOptim.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for ProgressiveBinningOptim.jl](https://pawelstrzebonski.gitlab.io/ProgressiveBinningOptim.jl/).
