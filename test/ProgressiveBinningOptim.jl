@testset "ProgressiveBinningOptim.jl" begin
    X = rand(8, 8)
    Y = rand(20, 12)

    @test ProgressiveBinningOptim.findnearest(1.23, 1:5) == 1
    @test ProgressiveBinningOptim.findnearest(1.99, 1:5) == 2

    res = 0
    @test (res = ProgressiveBinningOptim.remapper(size(X), size(Y)); true)
    @test size(X[res]) == size(Y)
    @test (res = ProgressiveBinningOptim.remapper(size(Y), size(X)); true)
    @test size(Y[res]) == size(X)

    # Basic test to see if anything fails
    #TODO: check proper functionality with actual optimization routine?
    costfun = x -> sum(x)
    optfun = (f, x) -> x
    @test (
        res = ProgressiveBinningOptim.progressive_binning_optim(X, costfun, optfun); true
    )
    @test size(res) == size(X)
end
